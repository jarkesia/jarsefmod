/*===============================================================================================
 Copyright (c), Jari Suominen 2008-2012.
===============================================================================================*/
//#include "../../../examples/common/wincompat.h"

#include "fmod_studio.hpp"
#include "fmod.hpp"
#include "common.h"
#include <vector>
#include <string>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <alsa/asoundlib.h>
//#include <fmodex/fmod_event.hpp>
//#include <fmodex/fmod_errors.h>
//#include "jarse.h"
#include <stdint.h>   /* Standard types */
#include <string.h>   /* String function definitions */
#include <fcntl.h>    /* File control definitions */
#include <errno.h>    /* Error number definitions */
#include <termios.h>  /* POSIX terminal control definitions */
#include <sys/ioctl.h>
#include <getopt.h>
#include <string>
#include <sstream>

static const int MAX_ENTRIES = 5;

struct CallbackInfo
{
    Common_Mutex mMutex;
    std::vector<std::string> mEntries;
};

FMOD_RESULT F_CALLBACK markerCallback(FMOD_STUDIO_EVENT_CALLBACK_TYPE type, FMOD_STUDIO_EVENTINSTANCE* event, void *parameters);
FMOD::Studio::EventDescription* eventDescription = NULL;
FMOD::Studio::EventInstance* eventInstance = NULL;

FMOD::Studio::System * sys = NULL;
FMOD::Studio::ParameterInstance * variationParameter = NULL;
FMOD::Studio::ParameterInstance * nextParameter = NULL;

CallbackInfo info;

const unsigned int HRJA_SERIAL_CHANNEL = 10; 
const unsigned int HRJA_SERIAL_SEGMENT_START = 12;

bool variationActive = false;

int midiin = 0;
int midiout = 0;
snd_seq_t *seq_handle;

/*
	Initializing ALSA MIDI.
	It is highly unlikely that you want to edit this or even check what it has eaten.
 */
snd_seq_t *open_seq() {

  snd_seq_t *seq_handle;
  int portid;

  if (snd_seq_open(&seq_handle, "hw", SND_SEQ_OPEN_DUPLEX, 0) < 0) {
    fprintf(stderr, "Error opening ALSA sequencer.\n");
    exit(1);
  }
  snd_seq_set_client_name(seq_handle, "hurja");
  if ((midiin = snd_seq_create_simple_port(seq_handle, "MIDI in",
            SND_SEQ_PORT_CAP_WRITE|SND_SEQ_PORT_CAP_SUBS_WRITE,
            SND_SEQ_PORT_TYPE_APPLICATION)) < 0) {
    fprintf(stderr, "Error creating sequencer port.\n");
    exit(1);
  }
  if ((midiout = snd_seq_create_simple_port(seq_handle, "MIDI out",
            SND_SEQ_PORT_CAP_READ|SND_SEQ_PORT_CAP_SUBS_READ,
            SND_SEQ_PORT_TYPE_APPLICATION)) < 0) {
    fprintf(stderr, "Error creating sequencer port.\n");
    exit(1);
  }
  return(seq_handle);
}


/*void ERRCHECK(FMOD_RESULT result) {
  if (result != FMOD_OK) {
    printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
    exit(-1);
  }
}*/

void next() {
  //eventsystem->update();
  //musicsystem->setParameterValue(MUSICPARAM_JARSE_VARIATION,1);
  //musicsystem->setParameterValue(MUSICPARAM_JARSE_NEXT,0);
  printf("NEXT!\n");
  //fflush(stdout);
	ERRCHECK(nextParameter->setValue(0.0f) );
	ERRCHECK( sys->update() );
}

void variation() {
//  eventsystem->update();
//  musicsystem->setParameterValue(MUSICPARAM_JARSE_NEXT,1);
//  musicsystem->setParameterValue(MUSICPARAM_JARSE_VARIATION,0);
  
  snd_seq_event_t ev;
	snd_seq_ev_clear(&ev);
	snd_seq_ev_set_direct(&ev);
	snd_seq_ev_set_source(&ev, midiout);
	snd_seq_ev_set_subs(&ev);
  snd_seq_ev_set_controller(&ev, HRJA_SERIAL_CHANNEL, 29, 1);
  snd_seq_event_output_direct(seq_handle, &ev);
	snd_seq_drain_output(seq_handle);

  printf("VARIATION!\n");
	ERRCHECK(variationParameter->setValue(0.0f) );
	ERRCHECK( sys->update() );
  fflush(stdout);
}

void reset() {
  //eventsystem->update();
  //musicsystem->setParameterValue(MUSICPARAM_JARSE_NEXT,1);
  //musicsystem->setParameterValue(MUSICPARAM_JARSE_VARIATION,1);
  //printf("RESET!\n");
  //fflush(stdout);
}

int previous_segment = -1;

/*FMOD_RESULT resetParams(FMOD_MUSIC_CALLBACKTYPE type, void * p1, void * p2, void * ud) {
	snd_seq_event_t ev;
	snd_seq_ev_clear(&ev);
	snd_seq_ev_set_direct(&ev);
	snd_seq_ev_set_source(&ev, midiout);
	snd_seq_ev_set_subs(&ev);

	int operation, channel, param1, param2;

	switch (type) {
		case FMOD_MUSIC_CALLBACKTYPE_BEAT:
		case 5:
			snd_seq_ev_set_noteon(&ev, 6, 0, 0);
      snd_seq_event_output_direct(seq_handle, &ev);
	    snd_seq_drain_output(seq_handle);
			//printf("BEAT!\n");
			//fflush(stdout);
			break;
		case FMOD_MUSIC_CALLBACKTYPE_SEGMENT_START:
			float f;
			musicsystem->getParameterValue(MUSICPARAM_JARSE_VARIATION,&f);
			if (f==0) {
				//write_int(51,fd);
			} else {
				//write_int(48,fd);
			}		
			if (previous_segment != (unsigned int)p1) {
				snd_seq_ev_set_controller(&ev, HRJA_SERIAL_CHANNEL, 25, (unsigned int)p1);
        snd_seq_event_output_direct(seq_handle, &ev);
	      snd_seq_drain_output(seq_handle);
				previous_segment = (unsigned int)p1;
			}
			printf("SEGMENT START! %i\n", (unsigned int)p1);
			reset();
			fflush(stdout);
			break;
		default:
			break;
	}
	
	return FMOD_OK;
}
*/

void loadSong(const char *path) {
	FMOD_STUDIO_PLAYBACK_STATE ps;
	if (eventInstance!=NULL) {	
		ERRCHECK( eventInstance->getPlaybackState(&ps) );
		if (ps==FMOD_STUDIO_PLAYBACK_PLAYING)	ERRCHECK( eventInstance->stop(FMOD_STUDIO_STOP_IMMEDIATE) );
	}
	ERRCHECK( sys->getEvent(path, &eventDescription) );
	ERRCHECK( eventDescription->createInstance(&eventInstance) );
	ERRCHECK( eventInstance->getParameter("Variation", &variationParameter) );
	ERRCHECK( eventInstance->getParameter("Progression", &nextParameter) );
	ERRCHECK( variationParameter->setValue(1.0f) );
	ERRCHECK( nextParameter->setValue(1.0f) );
	ERRCHECK( eventInstance->setUserData(&info) );
	ERRCHECK( eventInstance->setCallback(markerCallback, FMOD_STUDIO_EVENT_CALLBACK_TIMELINE_MARKER | FMOD_STUDIO_EVENT_CALLBACK_TIMELINE_BEAT) );
	ERRCHECK( eventInstance->start() );
}

/*
	This function processes the MIDI messages arriving from MIDI keyboard through ALSA MIDI.
	Only noteon and noteoff messages are used!
 */
void midi_action(snd_seq_t *seq_handle) {
	snd_seq_event_t *ev;

	do {
		snd_seq_event_input(seq_handle, &ev);
		if (ev->data.control.channel==0) {
			switch (ev->type) {
			case SND_SEQ_EVENT_PGMCHANGE:
				switch ((int)ev->data.control.value) {
					case 2: loadSong("event:/Music/Korpikuusen"); break;
					case 4: loadSong("event:/Music/Pussy");	break;
					case 5: variation(); break;
					case 6: next(); break;
					case 8: 
						bool b;
						ERRCHECK( eventInstance->getPaused(&b) );
						ERRCHECK( eventInstance->setPaused(!b) );
				}
				/*if ((int)ev->data.control.value==1) {
					//ERRCHECK(musicsystem->promptCue(MUSICCUE_JARSE_BO));
					//ERRCHECK(musicsystem->setPaused(false));
				} else if ((int)ev->data.control.value==2) {
					//ERRCHECK(musicsystem->promptCue(MUSICCUE_JARSE_KORPIKUUSEN));
					//ERRCHECK(musicsystem->setPaused(false));
				} else if ((int)ev->data.control.value==3) {
					//ERRCHECK(musicsystem->promptCue(MUSICCUE_JARSE_KOLME_DEE));
					//ERRCHECK(musicsystem->setPaused(false));
					//ERRCHECK(musicsystem->setPaused(true));
				} else if ((int)ev->data.control.value==7) {
					bool paused;
					//ERRCHECK(musicsystem->getPaused(&paused));
					//ERRCHECK(musicsystem->setPaused(!paused));
				
				} else if ((int)ev->data.control.value==5) {
					variation();
				} else if ((int)ev->data.control.value==6) {
					next();
				}*/
				break;
			}
		}
		else if (ev->data.control.channel==11) {
			switch (ev->type) {
      				case SND_SEQ_EVENT_NOTEON:
					if ((int)ev->data.note.note==2) {
						variation();
					} else if ((int)ev->data.note.note==1) {
						next();
					}
					break;
				case SND_SEQ_EVENT_CONTROLLER:
					printf("controller %i param %i \n",(int)ev->data.control.value,(int)ev->data.control.param);
					fflush(stdout);
					if ((int)ev->data.control.param==20&&(int)ev->data.control.value==127) {
						next();
					} else if ((int)ev->data.control.param==21&&(int)ev->data.control.value==127) {
						variation();
					} else if ((int)ev->data.control.param==22&&(int)ev->data.control.value==127) {
						//musicsystem->promptCue(MUSICCUE_JARSE_KORPIKUUSEN);
					} else if ((int)ev->data.control.param==23&&(int)ev->data.control.value==127) {
						//musicsystem->setParameterValue(MUSICPARAM_JARSE_NEXT,1);
						//musicsystem->setParameterValue(MUSICPARAM_JARSE_VARIATION,1);
					} 
					break; 
    			}
		}
		snd_seq_free_event(ev);
  	} while (snd_seq_event_input_pending(seq_handle, 0) > 0);
}

int main(int argc, char *argv[]) {

	void *extraDriverData = NULL;
	Common_Init(&extraDriverData);

	
	ERRCHECK( FMOD::Studio::System::create(&sys) );

	// The example Studio project is authored for 5.1 sound, so set up the system output mode to match
	FMOD::System* lowLevelSystem = NULL;
	ERRCHECK( sys->getLowLevelSystem(&lowLevelSystem) );
	ERRCHECK( lowLevelSystem->setSoftwareFormat(0, FMOD_SPEAKERMODE_5POINT1, 0) );
	//ERRCHECK( lowLevelSystem->setDSPBufferSize(256, 2) );
	ERRCHECK( lowLevelSystem->setOutput(FMOD_OUTPUTTYPE_ALSA));
	ERRCHECK( sys->initialize(32, FMOD_STUDIO_INIT_NORMAL, FMOD_INIT_NORMAL, extraDriverData) );

	
	// ALSA MIDI >>
  int npfd;
  struct pollfd *pfd;
  seq_handle = open_seq();
  npfd = snd_seq_poll_descriptors_count(seq_handle, POLLIN);
  pfd = (struct pollfd *)alloca(npfd * sizeof(struct pollfd));
  snd_seq_poll_descriptors(seq_handle, pfd, npfd, POLLIN);
	// ALSA MIDI <<

	FMOD::Studio::Bank* masterBank = NULL;
	ERRCHECK( sys->loadBankFile(Common_MediaPath("Master Bank.bank"), FMOD_STUDIO_LOAD_BANK_NORMAL, &masterBank) );

	FMOD::Studio::Bank* stringsBank = NULL;
	ERRCHECK( sys->loadBankFile(Common_MediaPath("Master Bank.strings.bank"), FMOD_STUDIO_LOAD_BANK_NORMAL, &stringsBank) );

//    FMOD::Studio::Bank* musicBank = NULL;
//    FMOD_RESULT result = system->loadBankFile(Common_MediaPath("Music.bank"), FMOD_STUDIO_LOAD_BANK_NORMAL, &musicBank);
 //   if (result != FMOD_OK)
 //   {
        // Music bank is not exported by default, you will have to export from the tool first
  //      Common_Fatal("Please export music.bank from the Studio tool to run this example");
  //  }

	
	Common_Mutex_Create(&info.mMutex);

	loadSong("event:/Music/Korpikuusen");
	ERRCHECK( eventInstance->stop(FMOD_STUDIO_STOP_IMMEDIATE) );

	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

	printf ("lines %d\n", w.ws_row);
	printf ("columns %d\n", w.ws_col);

  do { // MAIN LOOP
		Common_Update();
		ERRCHECK( sys->update() );

		int position;
		ERRCHECK( eventInstance->getTimelinePosition(&position) );

		Common_Draw("==================================================");
		Common_Draw("Music Callback Example.");
		Common_Draw("Copyright (c) Firelight Technologies 2015-2015.");
		Common_Draw("==================================================");
		Common_Draw("");
		Common_Draw("Timeline = %d", position);
		Common_Draw("");
		// Obtain lock and look at our strings
		Common_Mutex_Enter(&info.mMutex);
		for (size_t i=0; i<info.mEntries.size(); ++i)
		{
			Common_Draw("    %s\n", info.mEntries[i].c_str());
		}
		Common_Mutex_Leave(&info.mMutex);
		Common_Draw("");
		Common_Draw("Press %s to quit", Common_BtnStr(BTN_QUIT));
        	
    if (poll(pfd,npfd, 100) > 0) {
      midi_action(seq_handle);
    }

    Common_Sleep(5);
	
    //ERRCHECK(result = eventsystem->update());
    fflush(stdout);

  } while (!Common_BtnPress(BTN_QUIT));

	ERRCHECK( sys->release() );

	Common_Mutex_Destroy(&info.mMutex);
	Common_Close();

	return 0;
}

// Obtain a lock and add a string entry to our list
void markerAddString(CallbackInfo* info, const char* format, ...)
{
    char buf[256];
    va_list args;
    va_start(args, format);
    Common_vsnprintf(buf, 256, format, args);
    va_end(args);
    buf[255] = '\0';
    Common_Mutex_Enter(&info->mMutex);
    if (info->mEntries.size() >= MAX_ENTRIES)
    {
        info->mEntries.erase(info->mEntries.begin());
    }
    info->mEntries.push_back(std::string(buf));
    Common_Mutex_Leave(&info->mMutex);
}

// Callback from Studio - Remember these callbacks will occur in the Studio update thread, NOT the game thread.
FMOD_RESULT F_CALLBACK markerCallback(FMOD_STUDIO_EVENT_CALLBACK_TYPE type, FMOD_STUDIO_EVENTINSTANCE* event, void *parameters) {
	CallbackInfo* callbackInfo;
	ERRCHECK(((FMOD::Studio::EventInstance*)event)->getUserData((void**)&callbackInfo));

	if (type == FMOD_STUDIO_EVENT_CALLBACK_TIMELINE_MARKER) {
		FMOD_STUDIO_TIMELINE_MARKER_PROPERTIES* props = (FMOD_STUDIO_TIMELINE_MARKER_PROPERTIES*)parameters;
		markerAddString(callbackInfo, "Named marker '%s'", props->name);
		ERRCHECK(variationParameter->setValue(1.0f));
		ERRCHECK(nextParameter->setValue(1.0f));
	} else if (type == FMOD_STUDIO_EVENT_CALLBACK_TIMELINE_BEAT) {
		FMOD_STUDIO_TIMELINE_BEAT_PROPERTIES* props = (FMOD_STUDIO_TIMELINE_BEAT_PROPERTIES*)parameters;
		markerAddString(callbackInfo, "beat %d, bar %d (tempo %.1f %d:%d)", props->beat, props->bar, props->tempo, props->timeSignatureUpper, props->timeSignatureLower);
	}
	return FMOD_OK;
}
